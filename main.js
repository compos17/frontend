// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueGoodTable from 'vue-good-table'
import swal from 'sweetalert'

#sweetalert notification pop up
swal("example", {
  title: 'Warning',
  dangerMode: true,
  buttons: true,
});

Vue.config.productionTip = false

#smart table example
Vue.use(VueGoodTable)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
